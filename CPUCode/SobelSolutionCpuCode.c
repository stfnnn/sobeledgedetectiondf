/**
 * Document: MaxCompiler Training (maxcompiler-training.pdf)
 * Chapter: 2
 * Exercise Solution: 2
 * Summary:
 * 	 Performs SobelSolution edge detection on an image.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "Maxfiles.h"
#include <MaxSLiCInterface.h>

#include "ppmIO.h"

int main(void)
{
	printf("Loading image.\n");
	int32_t *input;
	int imageWidth = 0, imageHeight = 0;
	loadImage("lena.ppm", &input, &imageWidth, &imageHeight, 1);

	int size = imageWidth * imageHeight * sizeof(int);
	int32_t *output = malloc(size);

	SobelSolution(imageWidth * imageHeight, input, output);

	writeImage("lena_sobel.ppm", output, imageWidth, imageHeight, 1);

	return 0;
}
